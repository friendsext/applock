package com.priya.biz2credit.assignmentbiz2credit;

import android.app.ActivityManager;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class MainService extends Service {

    private static final String TAG = "priya";
    private static final int SHOW_TOAST = 11;
    private static final int SCHEDULE_POLLING = 12;
    private List<ResolveInfo> launcher;
    private Handler backgroundHandler;
    private Toast toast;
    public HashMap<String,String> map = new HashMap<>();
    private String currentAppName = "";

    private void createBackGroundHandler(HandlerThread backgroundthread) {
        backgroundHandler = new Handler(backgroundthread.getLooper()) {

            @Override
            public void handleMessage(Message msg) {
//            super.handleMessage(msg);
                switch (msg.what) {
                    case SCHEDULE_POLLING: {
                        findAppInfoViaFilter(true);
                        findRunningProcesses();
                        backgroundHandler.postDelayed(runnable, 1000);
                        break;
                    }
                }
            }
        };
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            backgroundHandler.sendEmptyMessage(SCHEDULE_POLLING);
        }
    };

    public MainService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        launcher = findAppList();
        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        HandlerThread backgroundthread = new HandlerThread("Backgroundthread");
        backgroundthread.start();
        createBackGroundHandler(backgroundthread);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ass", "started");
//        findAppList();
//        findAppInfoViaFilter(true);
        backgroundHandler.post(runnable);
//        readSystemLogs();
//        findRunningProcesses();
        return START_REDELIVER_INTENT;
    }

    public void findRunningProcesses() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : tasks) {
            Log.d(TAG, info.processName);
            if (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                if(map.containsKey(info.processName)) {
                    Log.d(TAG, "!!!!!!!!!!! hurray !!!!!!!!!!!!! " + info.processName);
                    String matchedValue = map.get(info.processName);
                    if(!currentAppName.equals(matchedValue)) {
                        showToast(matchedValue);
                        currentAppName = matchedValue;
                    }
                }
            }
        }
    }

    private void showToast(String msg) {
        Message hMsg = handler.obtainMessage();
        hMsg.what = SHOW_TOAST;
        hMsg.obj = msg;
        handler.sendMessageDelayed(hMsg, 500);
    }

    public void readSystemLogs() {
        Log.d(TAG, "readSystemLogs");
        try {
            Process mLogcatProc = null;
            BufferedReader reader = null;
            mLogcatProc = Runtime.getRuntime().exec(new String[]{"logcat", "-d"});

            reader = new BufferedReader(new InputStreamReader(mLogcatProc.getInputStream()));

            String line;
            final StringBuilder log = new StringBuilder();
            String separator = System.getProperty("line.separator");

            while ((line = reader.readLine()) != null) {
                log.append(line);
                log.append(separator);
            }
            String w = log.toString();
            Log.d(TAG, w);
            showToast(w);
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private String eventToString(int eventType) {
        switch (eventType) {
            case UsageEvents.Event.MOVE_TO_FOREGROUND:
                return "Foreground";

            case UsageEvents.Event.MOVE_TO_BACKGROUND:
                return "Background";

            case UsageEvents.Event.CONFIGURATION_CHANGE:
                return "Config change";

            case UsageEvents.Event.USER_INTERACTION:
                return "User Interaction";

            default:
                return "Unknown: " + eventType;
        }
    }

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(final Message msg) {
            Log.d(TAG, "msg is : "+ msg.obj);
            final String toastmsg = (String) msg.obj;
            switch (msg.what) {
                case SHOW_TOAST: {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            //Your UI code here
                            toast.setText(toastmsg);
                            toast.show();
                        }
                    });
                    break;
                }
                default:{
                    super.handleMessage(msg);
                }
            }

        }
    };

    protected List<ResolveInfo> findAppList() {
        final PackageManager pm = getPackageManager();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appList = pm.queryIntentActivities(mainIntent, 0);
        Collections.sort(appList, new ResolveInfo.DisplayNameComparator(pm));

        for (ResolveInfo temp : appList) {


            Log.v(TAG, "package and activity name = "
                    + temp.activityInfo.packageName + "    "
                    + temp.activityInfo.name);


        }
        return appList;
    }

    private void findAppInfoViaFilter(boolean findSystemApp){
        PackageManager pm = getApplication().getPackageManager();
        List<PackageInfo> list = pm.getInstalledPackages(0);

        for(PackageInfo pi : list) {
            ApplicationInfo ai = null;
            try {
                ai = pm.getApplicationInfo(pi.packageName, 0);
                if ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                    Log.d(TAG,">>>>>>packages is system package"+pi.packageName);
                }else {
                    Log.d(TAG,">>>>>>packages is<<<<<<<<" + ai.publicSourceDir);
                    map.put(ai.processName, (String) ai.loadLabel(pm));
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
